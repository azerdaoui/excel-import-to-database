<?php

    $file = fopen("test2.csv","r");
    $localhost   = 'localhost';
    $db_name     = 'imports';
    $db_username = 'root';
    $db_password = '';
    $fields = array();


    // if your csv not seprared by ","
    // pls change the first param of the explode function by your seprator ";"
    while (!feof($file))
    {
        array_push($fields, explode(',', fgets($file)));
    }


    try{
        $pdo = new PDO("mysql:host=". $localhost .";dbname=". $db_name, $db_username, $db_password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql=$pdo->prepare("DROP TABLE excel_table ");
        $sql->execute();

    } catch(PDOException $e){
        die("ERROR: Could not connect. " . $e->getMessage());
    }

    $sql = "";
    $fields_sql = "COL1, ";
    $fields_values_sql = "";
    
    $k = 2;
    for($i = 0; $i < count($fields[0]); $i++) 
    {
        if($i == count($fields[0]) - 1){
            $sql        .= "COL".$k." VARCHAR(255)";
            $fields_sql .= "COL".$k;
        } 
        else{
            $sql        .= "COL".$k." VARCHAR(255),";
            $fields_sql .= "COL".$k.',';
        }  
        $k++; 
    }
    // Attempt create table query execution
    try{
        $sql = "CREATE TABLE excel_table (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, COL1 VARCHAR(255), ". $sql ." )";   
        $pdo->exec($sql); 
        echo "Table created successfully.";
    } catch(PDOException $e){
        die("ERROR: Could not able to execute $sql. " . $e->getMessage());
    }


    $fields_values_sql = "(";
    for($i = 0; $i < count($fields); $i++)
    {
        // if you want to change the fields separator, 
        // pls change the first param of the implode function
        $fields_values_sql .= "'". implode(",", $fields[$i])."',";
        for($j = 0; $j < count($fields[0]); $j++)
        {
            if($j == count($fields[0]) - 1){
                $fields_values_sql .= "'".$fields[$i][$j]."'";
            } 
            else{
                $fields_values_sql .= "'". $fields[$i][$j]."',";
            }   
        }
        $fields_values_sql .= ")";
        $query = "INSERT INTO excel_table (". $fields_sql .") VALUES ". $fields_values_sql;
        // var_dump($query);die();
        $pdo->exec($query);
        $fields_values_sql = "(";
    }

    unset($pdo);
